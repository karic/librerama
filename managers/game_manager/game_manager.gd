###############################################################################
# Librerama                                                                   #
# Copyright (C) 2021 Michael Alexsander                                       #
#-----------------------------------------------------------------------------#
# This file is part of Librerama.                                             #
#                                                                             #
# Librerama is free software: you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# Librerama is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with Librerama.  If not, see <http://www.gnu.org/licenses/>.          #
###############################################################################

extends CanvasLayer


signal dim_changed

# TODO: Use global project version in Godot 4.0.
const VERSION = "0.5.0"

const SETTINGS_PATH = "user://settings.ini"

const REPOSITORY_LINK = "https://codeberg.org/Yeldham/librerama"

const FADE_SPEED = 0.25

const _NANOGAME_INPUT_ACTIONS = [
	"nanogame_up",
	"nanogame_down",
	"nanogame_left",
	"nanogame_right",
	"nanogame_action"
]

var dim := false setget set_dim

var _is_switching_scene := false

var _system_locale := ""

var _is_locale_system_default := false
var _is_turning_locale_system_default := false

var _was_muted := false


func _ready() -> void:
	OS.min_window_size = Vector2(133, 100)

	AudioServer.set_bus_volume_db(0, linear2db(0.5))
	AudioServer.set_bus_volume_db(1, linear2db(0.8))

	if OS.has_feature("release"):
		randomize()

	### Load Settings ###

	var config := ConfigFile.new()
	var error_code: int = config.load(SETTINGS_PATH)
	if error_code != OK:
		if error_code != ERR_FILE_NOT_FOUND:
			push_error("Unable to load settings data. Error code: " +
					str(error_code))
		else:
			save_settings()

		return

	if config.has_section_key("general", "language"):
		var value = config.get_value("general", "language", null)
		if value != null and value is String and not value.empty() and\
				ProjectSettings.get_setting(
						"locale/locale_filter")[1].has(value):
			TranslationServer.set_locale(value)
		else:
			set_locale_system_default()
	else:
		set_locale_system_default()

	if config.has_section_key("general", "fullscreen_window"):
		var value = config.get_value("general", "fullscreen_window", null)
		if value != null and value is bool:
			OS.window_fullscreen = value

	for i in ["pause_focus_lost", "mute_focus_lost", "show_community_warning"]:
		if config.has_section_key("general", i):
			var value = config.get_value("general", i, null)
			if value != null and value is bool:
				ProjectSettings.set_setting("game_settings/" + i, value)

	for i in _NANOGAME_INPUT_ACTIONS:
		var input_events: Array = InputMap.get_action_list(i)

		if config.has_section_key("controls", i + "_keyboard"):
			var value = config.get_value("controls", i + "_keyboard", null)
			if value != null and value is int and\
					OS.get_scancode_string(value) != "":
				for j in input_events:
					if j is InputEventKey:
						j.scancode = value

						break

		if config.has_section_key("controls", i + "_joypad"):
			var value = config.get_value("controls", i + "_joypad", null)
			if value != null and value is int and value >= 0 and\
					value <= JOY_BUTTON_MAX:
				for j in input_events:
					if j is InputEventJoypadButton:
						j.button_index = value

						break

	for i in ["force_touch_controls", "switch_touch_controls"]:
		if config.has_section_key("controls", i):
			var value = config.get_value("controls", i, null)
			if value != null and value is bool:
				ProjectSettings.set_setting("game_settings/" + i, value)

	for i in AudioServer.bus_count:
		var bus_name: String = AudioServer.get_bus_name(i).to_lower()

		if config.has_section_key("audio", bus_name + "_mute"):
			var value = config.get_value("audio", bus_name + "_mute", null)
			if value != null and value is bool:
				AudioServer.set_bus_mute(i, value)

				# Prevent unmuting of "Master" bus if the game started
				# unfocused.
				if i == 0:
					_was_muted = value

		if config.has_section_key("audio", bus_name + "_volume"):
			var value = config.get_value("audio", bus_name + "_volume", null)
			if value != null and value is float and value >= 0 and value <= 1:
				AudioServer.set_bus_volume_db(i, linear2db(value))


	save_settings()


func _notification(what: int) -> void:
	match what:
		NOTIFICATION_WM_FOCUS_IN:
			if not OS.has_feature("mobile") and not _was_muted and\
					ProjectSettings.get_setting(
							"game_settings/mute_focus_lost"):
				AudioServer.set_bus_mute(0, false)

			if _is_locale_system_default and _system_locale != OS.get_locale():
				set_locale_system_default()
		NOTIFICATION_WM_FOCUS_OUT:
			if not OS.has_feature("mobile") and ProjectSettings.get_setting(
					"game_settings/mute_focus_lost"):
				_was_muted = AudioServer.is_bus_mute(0)

				AudioServer.set_bus_mute(0, true)
		NOTIFICATION_TRANSLATION_CHANGED:
			if not _is_turning_locale_system_default:
				_is_locale_system_default = false


func switch_main_scene(
		to_scene: PackedScene, fade_color := ColorN("black")) -> void:
	if _is_switching_scene:
		push_error("The game manager is already switching the main scene.")

		return

	_is_switching_scene = true

	fade_color.a = 0
	var fade := $Fade as ColorRect
	fade.color = fade_color

	var tween := $Tween as Tween
	# warning-ignore:return_value_discarded
	tween.interpolate_property(fade, "color:a", 0, 1, FADE_SPEED)
	# warning-ignore:return_value_discarded
	tween.start()

	yield(tween, "tween_all_completed")
	# warning-ignore:return_value_discarded
	get_tree().change_scene_to(to_scene)

	_is_switching_scene = false

	# warning-ignore:return_value_discarded
	tween.interpolate_property(fade, "color:a", 1, 0, FADE_SPEED)
	# warning-ignore:return_value_discarded
	tween.start()


func save_settings() -> void:
	var config := ConfigFile.new()
	config.set_value("general", "language", TranslationServer.get_locale()
			if not _is_locale_system_default else "")

	config.set_value("general", "fullscreen_window", OS.window_fullscreen)

	for i in ["pause_focus_lost", "mute_focus_lost", "show_community_warning"]:
		config.set_value("general", i,
				ProjectSettings.get_setting("game_settings/" + i))

	for i in _NANOGAME_INPUT_ACTIONS:
		var input_events: Array = InputMap.get_action_list(i)

		for j in input_events:
			if j is InputEventKey:
				config.set_value("controls", i + "_keyboard", j.scancode)

				break

		for j in input_events:
			if j is InputEventJoypadButton:
				config.set_value("controls", i + "_joypad", j.button_index)

				break

	for i in ["force_touch_controls", "switch_touch_controls"]:
		config.set_value("controls", i,
				ProjectSettings.get_setting("game_settings/" + i))

	for i in AudioServer.bus_count:
		var bus_name: String = AudioServer.get_bus_name(i).to_lower()
		config.set_value(
				"audio", bus_name + "_mute", AudioServer.is_bus_mute(i))
		config.set_value("audio", bus_name + "_volume",
				db2linear(AudioServer.get_bus_volume_db(i)))

	var error_code: int = config.save(SETTINGS_PATH)
	if error_code != OK:
		push_error("Unable to save settings data. Error code: " +
				str(error_code))


func set_dim(is_visible: bool) -> void:
	if dim == is_visible:
		return

	dim = is_visible

	if get_tree().current_scene is CanvasItem:
		get_tree().current_scene.modulate =\
				ColorN("dimgray" if is_visible else "white")

	emit_signal("dim_changed")


func set_locale_system_default() -> void:
	_is_locale_system_default = true
	_is_turning_locale_system_default = true

	_system_locale = OS.get_locale()
	var languages: Array =\
			ProjectSettings.get_setting("locale/locale_filter")[1]
	if languages.has(_system_locale):
		TranslationServer.set_locale(_system_locale)
	else:
		var locale_bits: PoolStringArray = _system_locale.split("_")
		if not locale_bits.empty() and languages.has(locale_bits[0]):
			TranslationServer.set_locale(locale_bits[0])
		else:
			TranslationServer.set_locale("en")

	# Wait for the translation change notification to blow off first.
	yield(get_tree(), "idle_frame")
	_is_turning_locale_system_default = false


func is_locale_system_default() -> bool:
	return _is_locale_system_default


func get_nanogame_input_actions() -> Array:
	return _NANOGAME_INPUT_ACTIONS.duplicate()
