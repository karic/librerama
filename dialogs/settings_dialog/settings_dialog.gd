###############################################################################
# Librerama                                                                   #
# Copyright (C) 2021 Michael Alexsander                                       #
#-----------------------------------------------------------------------------#
# This file is part of Librerama.                                             #
#                                                                             #
# Librerama is free software: you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# Librerama is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with Librerama.  If not, see <http://www.gnu.org/licenses/>.          #
###############################################################################

extends TabDialog


const FONT_BOLD = preload("res://fonts/font_bold.tres")

var _language_button := OptionButton.new()

var _inputs_section := VBoxContainer.new()
var _input_buttons := ButtonGroup.new()
var _input_dialog :=\
		preload("res://dialogs/simple_dialog/simple_dialog.tscn").instance()\
		as SimpleDialog

var _switch_touch_controls := CheckBox.new()

var _volume_buttons := []


func _ready() -> void:
	var is_mobile: bool = OS.has_feature("mobile")

	### General ###

	var general := VBoxContainer.new()

	var language_container := HBoxContainer.new()
	general.add_child(language_container)

	var language_label := Label.new()
	# translation-extract:"Language:"
	language_label.text = "Language:"
	language_label.valign = Label.VALIGN_CENTER
	language_label.autowrap = true
	language_label.clip_text = true
	language_label.size_flags_horizontal = SIZE_EXPAND_FILL
	language_label.size_flags_vertical = SIZE_FILL
	language_label.add_font_override("font", FONT_BOLD)
	language_container.add_child(language_label)

	_language_button.clip_text = true
	_language_button.rect_min_size.x = 400
	# translation-extract:"System Default"
	_language_button.add_item("System Default")
	_language_button.add_separator()

	var file := File.new()
	# warning-ignore:return_value_discarded
	file.open("res://dialogs/settings_dialog/language_names.json", File.READ)
	var language_names: Dictionary = parse_json(file.get_as_text())
	file.close()

	var is_locale_system_default := GameManager.is_locale_system_default()
	var selected_id := 2 # Skip the "System Default" and the separator IDs.
	for i in ProjectSettings.get_setting("locale/locale_filter")[1]:
		_language_button.add_item(language_names[i])

		if is_locale_system_default:
			continue

		if i == TranslationServer.get_locale():
			_language_button.selected = selected_id
		else:
			selected_id += 1
	if is_locale_system_default:
		_language_button.selected = 0

	language_container.add_child(_language_button)
	# warning-ignore:return_value_discarded
	_language_button.connect(
			"item_selected", self, "_on_language_button_item_selected")

	if not is_mobile:
		var fullscreen_window := CheckBox.new()
		# translation-extract:"Fullscreen Window"
		fullscreen_window.text = "Fullscreen Window"
		fullscreen_window.clip_text = true
		fullscreen_window.pressed = OS.window_fullscreen
		general.add_child(fullscreen_window)
		# warning-ignore:return_value_discarded
		fullscreen_window.connect(
				"toggled", self, "_on_fullscreen_window_toggled")

	var pause_focus_lost := CheckBox.new()
	# translation-extract:"Pause When the Game Loses Focus"
	pause_focus_lost.text = "Pause When the Game Loses Focus"
	pause_focus_lost.clip_text = true
	pause_focus_lost.pressed =\
			ProjectSettings.get_setting("game_settings/pause_focus_lost")
	general.add_child(pause_focus_lost)
	# warning-ignore:return_value_discarded
	pause_focus_lost.connect(
			"toggled", self, "_on_pause_focus_lost_toggled")

	if not is_mobile:
		var mute_focus_lost := CheckBox.new()
		# translation-extract:"Mute When the Game Loses Focus"
		mute_focus_lost.text = "Mute When the Game Loses Focus"
		mute_focus_lost.clip_text = true
		mute_focus_lost.pressed =\
				ProjectSettings.get_setting("game_settings/mute_focus_lost")
		general.add_child(mute_focus_lost)
		# warning-ignore:return_value_discarded
		mute_focus_lost.connect("toggled", self, "_on_mute_focus_lost_toggled")

	var show_community_warning := CheckBox.new()
	# translation-extract:"Show Warning About Community Nanogames"
	show_community_warning.text = "Show Warning About Community Nanogames"
	show_community_warning.clip_text = true
	show_community_warning.pressed =\
			ProjectSettings.get_setting("game_settings/show_community_warning")
	general.add_child(show_community_warning)
	# warning-ignore:return_value_discarded
	show_community_warning.connect(
			"toggled", self, "_on_show_community_warning_toggled")

	# translation-extract:"General"
	add_tab("General", general)


	### Controls ###

	var controls := VBoxContainer.new()

	var nanogame_input_actions: Array =\
			GameManager.get_nanogame_input_actions()
	var inputs := {
		# translation-extract:"Up:"
		"Up:": nanogame_input_actions[0],
		# translation-extract:"Down:"
		"Down:": nanogame_input_actions[1],
		# translation-extract:"Left:"
		"Left:": nanogame_input_actions[2],
		# translation-extract:"Right:"
		"Right:": nanogame_input_actions[3],
		# translation-extract:"Action:"
		"Action:": nanogame_input_actions[4]
	}

	for i in inputs.keys():
		var container := HBoxContainer.new()
		_inputs_section.add_child(container)

		var label := Label.new()
		label.text = i
		label.clip_text = true
		label.size_flags_horizontal = SIZE_EXPAND_FILL
		label.add_font_override("font", FONT_BOLD)
		container.add_child(label)

		var input_button := Button.new()
		input_button.toggle_mode = true
		input_button.group = _input_buttons
		input_button.size_flags_horizontal = SIZE_EXPAND_FILL
		input_button.set_meta("action", inputs[i])
		container.add_child(input_button)
		# warning-ignore:return_value_discarded
		input_button.connect("pressed", _input_dialog, "popup_centered")

	controls.add_child(_inputs_section)

	_input_dialog.set_script(
			preload("res://dialogs/settings_dialog/input_dialog.gd"))
	_inputs_section.add_child(_input_dialog)
	# warning-ignore:return_value_discarded
	_input_dialog.connect("ok_pressed", self, "_on_input_dialog_ok_pressed")
	# warning-ignore:return_value_discarded
	_input_dialog.connect("hide", self, "_on_input_dialog_hide")

	if not is_mobile:
		var force_touch_controls := CheckBox.new()
		# translation-extract:"Force Touch Controls to Appear"
		force_touch_controls.text =\
				"Force Touch Controls to Appear"
		force_touch_controls.clip_text = true
		force_touch_controls.pressed = ProjectSettings.get_setting(
				"game_settings/force_touch_controls")
		controls.add_child(force_touch_controls)
		# warning-ignore:return_value_discarded
		force_touch_controls.connect(
				"toggled", self, "_on_force_touch_controls_toggled")

	# translation-extract:"Switch Touch Controls Positions"
	_switch_touch_controls.text = "Switch Touch Controls Positions"
	_switch_touch_controls.clip_text = true
	_switch_touch_controls.pressed = ProjectSettings.get_setting(
			"game_settings/switch_touch_controls")
	controls.add_child(_switch_touch_controls)
	# warning-ignore:return_value_discarded
	_switch_touch_controls.connect(
			"toggled", self, "_on_switch_touch_controls_toggled")

	var controls_scroll := ScrollContainer.new()
	controls_scroll.follow_focus = true
	controls.size_flags_horizontal = SIZE_EXPAND_FILL
	controls_scroll.add_child(controls)

	# warning-ignore:return_value_discarded
	Input.connect(
			"joy_connection_changed", self, "_on_Input_joy_connection_changed")
	_on_Input_joy_connection_changed(0, false)

	# translation-extract:"Controls"
	add_tab("Controls", controls_scroll)


	### Volume ###

	var volume := VBoxContainer.new()

	for i in AudioServer.bus_count:
		var container := HBoxContainer.new()
		volume.add_child(container)

		var label := Label.new()
		# translation-extract:"Master:"
		# translation-extract:"Music:"
		# translation-extract:"Effects:"
		label.text = AudioServer.get_bus_name(i) + ":"
		label.clip_text = true
		label.size_flags_horizontal = SIZE_EXPAND_FILL
		label.add_font_override("font", FONT_BOLD)
		container.add_child(label)

		var volume_button := CheckButton.new()
		volume_button.pressed = not AudioServer.is_bus_mute(i)
		_volume_buttons.append(volume_button)
		container.add_child(volume_button)
		# warning-ignore:return_value_discarded
		volume_button.connect(
				"toggled", self, "_on_volume_button_toggled", [i])

		var slider := HSlider.new()
		slider.max_value = 1
		slider.step = 0.01
		slider.value = db2linear(AudioServer.get_bus_volume_db(i))
		volume.add_child(slider)
		# warning-ignore:return_value_discarded
		slider.connect(
				"value_changed", self, "_on_volume_slider_value_changed", [i])

	_update_volume_buttons()

	# translation-extract:"Volume"
	add_tab("Volume", volume)


	# warning-ignore:return_value_discarded
	connect("tab_changed", self, "_on_tab_changed")


func _update_volume_buttons() -> void:
	for i in _volume_buttons.size():
		_volume_buttons[i].text = str(int(db2linear(
				AudioServer.get_bus_volume_db(i)) * 100)) + "%"


func _on_tab_changed(tab: int) -> void:
	match tab:
		0: # "General".
			_language_button.grab_focus()
		1: # "Controls".
			if OS.has_feature("mobile"):
				if not Input.get_connected_joypads().empty():
					_switch_touch_controls.grab_focus()

					return

			_input_buttons.get_buttons()[0].grab_focus()
		2: # "Volume".
			_volume_buttons.front().grab_focus()


func _on_language_button_item_selected(id: int) -> void:
	if id == 0:
		GameManager.set_locale_system_default()
	else:
		TranslationServer.set_locale(ProjectSettings.get_setting(
				"locale/locale_filter")[1][id - 2])

	GameManager.save_settings()


func _on_fullscreen_window_toggled(button_pressed: bool) -> void:
	OS.window_fullscreen = button_pressed

	GameManager.save_settings()


func _on_pause_focus_lost_toggled(button_pressed: bool) -> void:
	ProjectSettings.set_setting(
			"game_settings/pause_focus_lost", button_pressed)

	GameManager.save_settings()


func _on_mute_focus_lost_toggled(button_pressed: bool) -> void:
	ProjectSettings.set_setting(
			"game_settings/mute_focus_lost", button_pressed)

	GameManager.save_settings()


func _on_show_community_warning_toggled(button_pressed: bool) -> void:
	ProjectSettings.set_setting(
			"game_settings/show_community_warning", button_pressed)

	GameManager.save_settings()


func _on_force_touch_controls_toggled(button_pressed: bool) -> void:
	ProjectSettings.set_setting(
			"game_settings/force_touch_controls", button_pressed)

	GameManager.save_settings()


func _on_switch_touch_controls_toggled(button_pressed: bool) -> void:
	ProjectSettings.set_setting(
			"game_settings/switch_touch_controls", button_pressed)

	GameManager.save_settings()


func _on_volume_button_toggled(button_pressed: bool, bus: int) -> void:
	AudioServer.set_bus_mute(bus, not button_pressed)

	GameManager.save_settings()


func _on_volume_slider_value_changed(value: float, bus: int) -> void:
	AudioServer.set_bus_volume_db(bus, linear2db(value))

	_update_volume_buttons()

	GameManager.save_settings()


func _on_input_dialog_ok_pressed() -> void:
	var input_button: Button = _input_buttons.get_pressed_button()
	var has_joypads: bool = not Input.get_connected_joypads().empty()
	for i in InputMap.get_action_list(input_button.get_meta("action")):
		# Append zero-width space at the end to avoid automatic translation.
		if has_joypads:
			if i is InputEventJoypadButton:
				input_button.text = Input.get_joy_button_string(
						_input_dialog.get_input_index()) + char(8203)

				i.button_index = _input_dialog.get_input_index()

				break
		elif i is InputEventKey:
			input_button.text =\
					OS.get_scancode_string(_input_dialog.get_input_index()) +\
							char(8203)

			i.scancode = _input_dialog.get_input_index()

			break

	GameManager.save_settings()


func _on_input_dialog_hide() -> void:
	_input_buttons.get_pressed_button().pressed = false


func _on_Input_joy_connection_changed(_device: int, _connected: bool) -> void:
	if _input_dialog.visible:
		_input_dialog.hide()

	var has_joypads: bool = not Input.get_connected_joypads().empty()
	if OS.has_feature("mobile") and not has_joypads:
		_inputs_section.hide()

		return

	for i in _input_buttons.get_buttons():
		i.text = ""

		# Append zero-width space at the end to avoid automatic translation.
		for j in InputMap.get_action_list(i.get_meta("action")):
			if has_joypads:
				if j is InputEventJoypadButton:
					i.text = Input.get_joy_button_string(j.button_index) +\
							char(8203)

					break
			elif j is InputEventKey:
				i.text = OS.get_scancode_string(j.scancode) + char(8203)

				break

	_inputs_section.show()
