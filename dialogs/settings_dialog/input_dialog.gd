###############################################################################
# Librerama                                                                   #
# Copyright (C) 2021 Michael Alexsander                                       #
#-----------------------------------------------------------------------------#
# This file is part of Librerama.                                             #
#                                                                             #
# Librerama is free software: you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# Librerama is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with Librerama.  If not, see <http://www.gnu.org/licenses/>.          #
###############################################################################

extends SimpleDialog


var _has_joypads := false

var _input_index := -1


func _ready() -> void:
	update_on_translation_change = false

	rect_size = Vector2(700, 300)

	set_confirmation_mode(true)

	set_process_input(false)


func _notification(what: int) -> void:
	match what:
		NOTIFICATION_POST_POPUP:
			_has_joypads = not Input.get_connected_joypads().empty()

			_input_index = -1

			_update_text()

			set_ok_enabled(false)

			set_process_input(true)
		NOTIFICATION_POPUP_HIDE:
			set_process_input(false)
		NOTIFICATION_TRANSLATION_CHANGED:
			_update_text()


func _input(event: InputEvent) -> void:
	if _has_joypads:
		if not event is InputEventJoypadButton:
			return
	elif not event is InputEventKey:
		return

	# Don't allow binding inputs that are already being used.
	for i in GameManager.get_nanogame_input_actions() + ["pause", "ui_cancel"]:
		if event.is_action(i):
			_update_text(true)

			return

	_input_index = event.button_index if _has_joypads else event.scancode
	accept_event()

	_update_text()

	set_ok_enabled(true)

	set_process_input(false)


func _update_text(is_invalid:=false) -> void:
	var instructions := ""
	if is_invalid:
		# translation-extract:"Invalid or already in use, try another."
		instructions = "Invalid or already in use, try another."
	elif _input_index == -1:
		# translation-extract:"Press the desired new input in your joypad."
		# translation-extract:"Press the desired new input in your keyboard."
		instructions = "Press the desired new input in your joypad."\
				if _has_joypads else\
				"Press the desired new input in your keyboard."
	else:
		instructions = "Is this the new desired input?"

	var input_status := ""
	if is_invalid:
		# translation-extract:"[Invalid Input]"
		input_status = "[color=#" + ColorN("tomato").to_html() + "]" +\
				tr("[Invalid Input]") + "[/color]"
	elif _input_index == -1:
		# translation-extract:"[None Selected]"
		input_status = "[color=silver]" + tr("[None Selected]") + "[/color]"
	else:
		input_status = Input.get_joy_button_string(_input_index)\
				if _has_joypads else OS.get_scancode_string(_input_index)

	set_bbcode_text("[center]" + tr(instructions) + "\n\n[b]" + input_status +
			"[/b][/center]")


func get_input_index() -> int:
	return _input_index
